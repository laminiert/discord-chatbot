import tensorflow
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
import nltk
from nltk.stem.porter import PorterStemmer
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk import corpus
import csv
import pandas as pd
import numpy as np
import pickle
import discord
from discord.ext import commands
from dotenv import load_dotenv
from re import X
import asyncio
import io
import os
import json



intents = json.loads(open('intents_file.json').read())                      # loading the intents file for classification
stemmer = PorterStemmer()

words = []
classes = []
documents = []
ignore_letters = ['?','!','.', ',', '(', ')', '/', ':', ';', '&']           # ridding the json file of special caracters


for intent in intents['intents']:                                           # tokenizing, stemming and appending the json file
    for pattern in intent['patterns']:
        word_list = nltk.word_tokenize(pattern)
        slur_list = [stemmer.stem(word.lower()) for word in word_list if word not in ignore_letters]
        words.extend(slur_list)
        documents.append((slur_list, intent['tag'], intent['responses']))
        if intent['tag'] not in classes:
            classes.append(intent['tag'])


words = sorted(set(words))
classes = sorted(set(classes))


load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')                                          # loading the discord user and server numbers
class MyClient(discord.Client):                                             # this way the bot know what user to use and
                                                                            # which server to connect to

    async def on_ready(self):
        print('Logged in as', self.user)                                    # logging the bot onto the server

    
    


    async def on_message(self, message):                                    # placing bot in the "on_message" status
                                                                            # which makes it react to every message
        
        if message.content == '++':                                         # this command would add new words to the
                                                                            # json file, but does not work yet.
            try:
            
                await message.channel.send(f"Add a word: ")
                msg = await client.wait_for("msg", check=None, timeout=20)
                new_word = msg.content
                await message.channel.send(f"Add class: sexism, racism, queermisia or other ")
                word_class = await client.wait_for("class", check=None, timeout=10)
                new_class = word_class.lower()
                words.extend(new_word)

                if new_class == 'sexism':
                    documents.append(new_word, "sexism",
                             "You are using misogynist language. To learn more, you can visit this website: website, or visit our lexicon: lexicon")
                    await message.channel.send("Word added!")
                if new_class == 'racism':
                    documents.append(new_word, "racism",
                             "You are using racist language. To learn more, you can visit this website: website, or visit our lexicon: lexicon")
                    await message.channel.send("Word added!")
                if new_class == 'queermisia':
                    documents.append(new_word, "queermisia",
                             "You are using offensive or hateful language against queer people.To learn more, you can visit this website: website, or visit our lexicon: lexicon")
                    await message.channel.send("Word added!")
                if new_class == 'other':
                    documents.append(new_word, "other",
                             "You are using ableist, antisemitist or other kind of hateful language. To learn more, you can visit this website: website, or visit our lexicon: lexicon")
                    await message.channel.send("Word added!")
                else:
                    await message.channel.send(
                "Class not found. We only have the classes: sexism, racism, queermisia and other.")

            except asyncio.TimeoutError:
                await message.channel.send("Sorry, you didn't reply in time!")

           
        

        self.model = tensorflow.keras.models.load_model("chatbod",          # loading the keras model
                                                        compile=True)
        

        with open('tokeniser.pickle', 'rb') as handle:                      # loading the tokenizer
            tokenizer = pickle.load(handle)

        


        if message.author == self.user:                                     # this makes the bot ignore its own messages
            return

        
        messageContent = message.content.lower()                            # turning the recieve message into lowercase
        messageContent = messageContent.replace('?', "")                    # ridding the message of special signs
        messageContent = messageContent.replace('!', "")
       

        stop_words = set(stopwords.words('english'))
        
        word_tokens = word_tokenize(messageContent)
        print(word_tokens)
 

        filtered_sentence = []

        

        for w in word_tokens:                                               # removing all stopwords from the message
            if w not in stop_words:
                filtered_sentence.append(w)
 
        def get_stemmed_text(filtered_sentence):
            stemmer = PorterStemmer()
            filtered_sentence = [[stemmer.stem(word)                        # stemming the filtered sentence
                                  for word in sentence.split(" ")]
                                 for sentence in filtered_sentence]
            filtered_sentence = [" ".join(word) for word in filtered_sentence]
            return filtered_sentence
        print(get_stemmed_text(filtered_sentence))
        X = get_stemmed_text(filtered_sentence)
        print(type(X))
        
    
        Xy = np.array(X)
        
        print(Xy)
        tokenizer.fit_on_texts(Xy)                                          # removing all stopwords from the message
        train_sequences = tokenizer.texts_to_sequences(Xy)                  # turning sentence into sequence
        print(train_sequences)

        train_sequences = pad_sequences(train_sequences, maxlen=33,         # padding to the shape of the model
                                        dtype='int32',padding='post')
        print(train_sequences.shape)
        print(train_sequences)
        Xyz = np.array(train_sequences)
      

        print(Xyz)
        result = self.model.predict(Xyz)                                    # using the model to predict the offensiveness
        print(result)
        
        new_result = result*100                                             # simplifying the results for further use
        print(new_result)
        new_result = new_result.astype(int)
        print(new_result)
        l = len(new_result)
        for i in new_result:
            if i == 0 and train_sequences.any() != 0:                       # sorting between offensive and non offensive
                print (i)
                slur = []
                slur_classes = []
                slur_responses = []
                offensive = False

                for word in X:
                    for tuple in documents:
                        if word in tuple[0]:
                            offensive = True
                            slur.append(tuple[0])
                        if tuple[1] not in slur_classes:
                                slur_classes.append(tuple[1])
                        if tuple[2] not in slur_responses:
                            slur_responses.append(tuple[2])
                if offensive == True:                                   # specifiying the bots reaction to an offensive message

                    await message.author.send('Problematic words used: {0}'.format(', '.join(map(str, slur))))
                    await message.author.send('Classes of hate speech: {0}'.format(', '.join(map(str, slur_classes))))
                    await message.author.send('I recognized hateful language. {0}'.format('.\n '.join(map(str, slur_responses))))
                    break

                else:

                    await message.author.send("I detected hateful language, but can't recognize the class. To educate yourself about hate speech in general you can visit this website: https://blog.ongig.com/diversity-and-inclusion/discriminatory-language-in-job-descriptions/ and also visit our lexicon on gitlab: https://gitlab.com/laminiert/discord-chatbot/-/tree/main/Lexicon \n "
                                   "Websites about sexist language: \n http://sacraparental.com/2016/05/14/everyday-misogyny-122-subtly-sexist-words-women/ \n https://www.coe.int/en/web/human-rights-channel/stop-sexism \n "
                                   "Websites about racist language: \n http://rsdb.org/races https://www.pcc.edu/illumination/wp-content/uploads/sites/54/2018/05/racism-in-the-english-language.pdf \n https://www.npr.org/2020/08/24/905515398/not-racist-is-not-enough-putting-in-the-work-to-be-anti-racist?t=1631094264104 \n"
                                   "Websites about queermisic language: \n https://www.ilga-europe.org/sites/default/files/challenging_homophobic_language.pdf \n https://www.stonewall.org.uk/sites/default/files/tackling_homophobic_language_-_teachers_guide.pdf \n"
                                   "Websites about ableist language: \n http://web.augsburg.edu/english/writinglab/Avoiding_Ableist_Language.pdf \n https://libguides.ufv.ca/c.php?g=705905&p=5022576 \n"
                                   "A website about antisemitist language: \n https://global.ajc.org/files/ajc/upload/AJC_Glossary.pdf")
			

                      
            else:
                print ('nicht offensive')
                print(i)

    
        
client = MyClient()
client.run(TOKEN)