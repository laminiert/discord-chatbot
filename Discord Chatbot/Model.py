import tensorflow
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.layers import InputLayer
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers  import Dense, Dropout
from tensorflow.keras.optimizers import Adam
import nltk
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
import pandas as pd
import numpy as np
from numpy import loadtxt
import string
import csv
nltk.download('wordnet')
nltk.download('punkt')


# copy the tweets

data = pd.read_csv('tweets.csv')
data_col = data['tweets'].copy()

printable = set(string.printable)
data_col['cleantext'] = data['tweets'].apply(lambda row: ''.join(filter(lambda x:x in printable,row)))


# make text to lowerCase

def pre_process(tweets):
    tweets = tweets.lower()

    return tweets

data_col['cleantext'] = data_col['cleantext'].apply(lambda x:pre_process(x))


# if wanted exclude Stopwords
# this was not used in the final version

english_stop_words = stopwords.words('english')
def remove_stop_words(corpus):
    removed_stop_words = []
    for review in corpus:
        removed_stop_words.append(
            ' '.join([word for word in review.split() 
                      if word not in english_stop_words])
        )
    return removed_stop_words


# Stemmatization

def get_stemmed_text(corpus):
    stemmer = PorterStemmer()
    return [' '.join([stemmer.stem(word) for word in review.split()]) for review in corpus]

data_col['stemmedtext'] = get_stemmed_text(data_col['cleantext'])



# write stemmatized text to a new or excisting csv file

def write_to_csv(list_of_tweets):
    with open('sample.csv', 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter='\n')
        writer.writerow(list_of_tweets)
write_to_csv(data_col['stemmedtext'])

file_CSV = open('sample.csv')
list_CSV = pd.read_csv("sample.csv",
                       index_col=None, names=["tweet"])
tweets = list_CSV["tweet"].to_list()

tokenizer = Tokenizer()
tokenizer.fit_on_texts(tweets)                              # tokenizing all tweets
train_sequences = tokenizer.texts_to_sequences(tweets)      # Converting text to a vector of word indexes
word_index = tokenizer.word_index
print('Found %s unique tokens.' % len(word_index))
print('1st token-id sequnce', train_sequences[0])
maxlen = max([len(x) for x in train_sequences])             # fetching the max length to pad to correct size

trainvalid_data_pre =  pad_sequences(train_sequences,       # padding
    maxlen=maxlen, dtype='int32', padding='post')

def write_to_csv(list_of_tweets):                           # saving the data again
    with open('ready_traindata.csv', 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter='\n')
        writer.writerow(list_of_tweets)
write_to_csv(trainvalid_data_pre)


# load the dataset

dataset1 = np.array(trainvalid_data_pre)
print(dataset1)
dataset = pd.read_csv('mycsv.csv',                          # loading the output data
                      delimiter=',', index_col=None)
dataset2 = dataset['tw'].copy()
print('')
print(len(dataset2))                                        # testing for proper dimensions
print('')


X = trainvalid_data_pre                                     # designating input data
print (np.shape(X))
print(X)

tokenizers=Tokenizer()
tokenizers.fit_on_texts(dataset2)                           # this is a workaround to get our input data to function.
dataset2 = np.array(tokenizers.texts_to_sequences(dataset2))-1

print(dataset2)

y = np.array(dataset2)                                      # designating output data


print(np.shape(y))                                          # checking for proper shape
print(y)
print(len(X[0]))
print(len(y))
print(len(dataset1))
print(len(dataset2))
print('')
print(len(trainvalid_data_pre))
 

model = Sequential()
model.add(InputLayer(input_shape=(33)))                     # defining model
model.add(Dense(256, activation='tanh'))
model.add(Dense(128, activation='tanh'))
model.add(Dense(64, activation='tanh'))
model.add(Dense(32, activation='tanh'))
model.add(Dense(1, activation='sigmoid'))


adam = Adam(learning_rate=0.001)                            # defining optimizer
model.compile(loss='mse', optimizer= adam,
              metrics=['accuracy'])
model.fit(X, y, epochs=500, batch_size=256)                 # fitting the model
model.summary()
print(model.output_shape)
model.save("chatbod")                                       # saving the model in keras for further use


import pickle
with open('tokeniser.pickle', 'wb') as handle:              # saving tokenizer for futher use
    pickle.dump(tokenizer, handle,
                protocol=pickle.HIGHEST_PROTOCOL)

